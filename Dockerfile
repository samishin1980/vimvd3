FROM golang: 1.16

RUN mkdir /app
WORKDIR /app

COPY ./ /app

RUN go build

RUN mkdir /app/resoureces
COPY app/resources/index.html /app/resources/index.html

EXPOSE 8000

CMD /app/pig
